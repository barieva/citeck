package ru.citeck;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.stream.Collectors.toList;

public class ArrayValuesSorter {
    public static final int MET_FIRST_TIME = 1;

    public static List<Map.Entry<Integer, AtomicInteger>> sort(int[] numbers) {
        Map<Integer, AtomicInteger> numbersAndTheirCountMap = new HashMap<>();
        for (int number : numbers) {
            if (numbersAndTheirCountMap.containsKey(number)) {
                numbersAndTheirCountMap.get(number).getAndIncrement();
            } else {
                numbersAndTheirCountMap.put(number, new AtomicInteger(MET_FIRST_TIME));
            }
        }
        return sortByValues(numbersAndTheirCountMap);
    }

    private static List<Map.Entry<Integer, AtomicInteger>> sortByValues(Map<Integer, AtomicInteger> numbers) {
        return numbers.entrySet()
                .stream().sorted(getValuesComparator()).collect(toList());
    }

    private static Comparator<Map.Entry<Integer, AtomicInteger>> getValuesComparator() {
        return (firstEntry, secondEntry) -> {
            if (firstEntry.getValue().get() > secondEntry.getValue().get()) {
                return 1;
            } else if (firstEntry.getValue().get() == secondEntry.getValue().get()) {
                return 0;
            } else {
                return -1;
            }
        };
    }
}