# a. standard

SQL2016, MySQL 8.0.

# b. scenario for table generation and data insertion

create table number_table(
    id int not null primary key,
    number int null
);

insert into number_table
values (1, 1),
       (2, 2),
       (3, 4),
       (4, 7),
       (5, 8),
       (6, 11);

# c. scenario for solving the task

select skippingNumber, (number - skippingNumber) as numberOfTimes
from (select number,
             @counter := @counter + 1 as                  skippingNumber,
             if(number = @counter, 0, @counter := number) differenceNumber
      from number_table,
           (select @counter := 0) counter) f
where differenceNumber > 0;