package ru.citeck;

import java.util.ArrayDeque;
import java.util.Deque;

public class RegularExpressionValidator {
    public static boolean validate(String expression) {
        Deque<Character> brackets = new ArrayDeque<>();
        for (int i = 0; i < expression.length(); i++) {
            char bracket = expression.charAt(i);
            if (bracket == '(' || bracket == '[') {
                brackets.push(bracket);
                continue;
            }
            if (brackets.isEmpty()) {
                return false;
            }
            switch (bracket) {
                case ')':
                    Character openingBracket = brackets.pop();
                    if (openingBracket == '[') {
                        return false;
                    }
                    break;
                case ']':
                    Character otherOpeningBracket = brackets.pop();
                    if (otherOpeningBracket == '(') {
                        return false;
                    }
                    break;
                default:
                    break;
            }
        }
        return brackets.isEmpty();
    }
}