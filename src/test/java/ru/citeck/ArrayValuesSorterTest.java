package ru.citeck;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static ru.citeck.ArrayValuesSorter.sort;

class ArrayValuesSorterTest {
    @Test
    void sortArray() {
        int[] numbers = {3, 3, 2, 2, 3, 1};
        List<Entry<Integer, AtomicInteger>> sortedNumbers = sort(numbers);
        assertEquals(1, sortedNumbers.get(0).getKey(), "Expected first number as one");
        assertEquals(3, sortedNumbers.get(2).getValue().get(), "Expected last count of number as three");
    }

    @Test
    void sortArrayWithOneNumber() {
        int[] numbers = {5};
        List<Entry<Integer, AtomicInteger>> sortedNumbers = sort(numbers);
        assertEquals(1, sortedNumbers.size(), "Expected size is one");
        assertEquals(5, sortedNumbers.get(0).getKey(), "There is only five");
        assertEquals(1, sortedNumbers.get(0).getValue().get(), "There is only one distinct five");
    }

    @Test
    void sortEmptyArray() {
        int[] numbers = {};
        List<Entry<Integer, AtomicInteger>> sortedNumbers = sort(numbers);
        assertEquals(0, sortedNumbers.size(), "Expected size is zero");
    }

    @Test
    void sortArrayWithDistinctNumbers() {
        int[] numbers = {4, 3, 2, 1};
        List<Entry<Integer, AtomicInteger>> sortedNumbers = sort(numbers);
        assertEquals(4, sortedNumbers.size(), "Expected size is four");
        sortedNumbers.stream()
                .forEach((entry) -> assertEquals(1, entry.getValue().get(), "There is only one distinct numbers"));
    }
}