package ru.citeck;

import org.junit.jupiter.api.Test;

import static java.lang.Integer.MAX_VALUE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static ru.citeck.NumberConverter.setFirstZeroBitToOne;

class NumberConverterTest {
    @Test
    void convertOddNumber() {
        int evenNumber = 0;
        setFirstZeroBitToOne(evenNumber);
        assertEquals(0b1, setFirstZeroBitToOne(evenNumber), "First right bit was 0 but it`ve not converted to one");
    }

    @Test
    void convertEvenNumber() {
        int evenNumber = 0b101;
        setFirstZeroBitToOne(evenNumber);
        assertEquals(0b111, setFirstZeroBitToOne(evenNumber), "Second right bit was 0 but it`ve not converted to one");
    }

    @Test
    void convertMaxIntegerNumber() {
        int evenNumber = MAX_VALUE;
        setFirstZeroBitToOne(evenNumber);
        assertEquals(-1, setFirstZeroBitToOne(evenNumber), "Sign bit was 0 but it`ve not converted to one");
    }

    @Test
    void convertNumberWithAllOneBits() {
        int evenNumber = -1;
        setFirstZeroBitToOne(evenNumber);
        assertEquals(-1, setFirstZeroBitToOne(evenNumber), "All bits of number was as one, but they converted");
    }
}