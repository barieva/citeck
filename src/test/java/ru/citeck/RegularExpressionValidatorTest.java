package ru.citeck;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static ru.citeck.RegularExpressionValidator.validate;

class RegularExpressionValidatorTest {

    @Test
    void validateRightCaseOfExpressionFromTask() {
        String rightBracketsNumber = "([][[]()])";
        assertEquals(true, validate(rightBracketsNumber), "Regular expression was right");
    }

    @Test
    void validateWrongCaseOfExpressionFromTask() {
        String wrongBracketsNumber = "([][]()])";
        assertEquals(false, validate(wrongBracketsNumber), "Regular expression was wrong");
    }
}